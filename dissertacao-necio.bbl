\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{1.53 }

\bibitem[Barbosa e Forster 2010]{barbosa2010sistemas}
\abntrefinfo{Barbosa e Forster}{BARBOSA; FORSTER}{2010}
{BARBOSA, D{\'e}bora Cristina~Modesto; FORSTER, Alda{\'\i}sa~Cassanho. Sistemas
  de informa{\c{c}}{\~a}o em sa{\'u}de: a perspectiva ea avalia{\c{c}}{\~a}o
  dos profissionais envolvidos na aten{\c{c}}{\~a}o prim{\'a}ria {\`a}
  sa{\'u}de de ribeir{\~a}o preto, s{\~a}o paulo.
\emph{Cad. saude colet}, p. 424--33, 2010.}

\bibitem[Boissier 1993]{boissier1993probleme}
\abntrefinfo{Boissier}{BOISSIER}{1993}
{BOISSIER, Olivier.
\emph{Probl{\`e}me du contr{\^o}le dans un syst{\`e}me int{\'e}gr{\'e} de
  vision: utilisation d'un syst{\`e}me multi-agents}.
Tese (Doutorado) --- Institut National Polytechnique de Grenoble-INPG, 1993.}

\bibitem[Brasil {} 1988]{constituicao88}
\abntrefinfo{Brasil}{BRASIL}{1988}
{BRASIL {}. \emph{Constituição da República Federativa do Brasil: promulgada
  em 5 de outubro de 1988. Organização do texto: Juarez de Oliveira. 4. ed.
  São Paulo: Saraiva, 1990. 168p. (Série Legislação Brasileira)}.
Brasılia, DF, 1988.}

\bibitem[BRASIL 2012]{pnab2012}
\abntrefinfo{BRASIL}{BRASIL}{2012a}
{BRASIL, Ministério da~Saúde. \emph{{Política Nacional de Atenção
  Básica}}. [S.l.]: Ministério da Saúde: (Séria E. Legislação em Saúde),
  2012.
ISBN 978-85-334-1939-1.}

\bibitem[BRASIL 2012]{brasil2012}
\abntrefinfo{BRASIL}{BRASIL}{2012b}
{BRASIL, Ministério da~Saúde. \emph{{Programa nacional de melhoria do acesso
  e da qualidade da atenção básica (PMAQ): manual instrutivo}}. [S.l.]:
  Ministério da Saúde: (Série A. Normas e Manuais Técnicos), 2012.}

\bibitem[Braun et al. 2005]{braun2005agent}
\abntrefinfo{Braun et al.}{BRAUN et al.}{2005}
{BRAUN, L. et al. Agent support in medical information retrieval. In:
  \emph{Working notes of the IJCAI-05. Workshop on agents applied in health
  care}. [S.l.: s.n.], 2005. p. 16--25.}

\bibitem[Bueno 2010]{bueno2010heuristicas}
\abntrefinfo{Bueno}{BUENO}{2010}
{BUENO, Marcos Luiz de~Paula. Heur{\'\i}sticas e algoritmos evolutivos para
  formula{\c{c}}{\~o}es mono e multiobjetivo do problema do roteamento
  multicast.
2010.}

\bibitem[Cormen 2002]{cormen2002algoritmos}
\abntrefinfo{Cormen}{CORMEN}{2002}
{CORMEN, Thomas~H. \emph{Algoritmos: teoria e pr{\'a}tica}. [S.l.]: Elsevier,
  2002.}

\bibitem[Freitas et al. 2010]{freitasde2010otimizaccao}
\abntrefinfo{Freitas et al.}{FREITAS et al.}{2010}
{FREITAS, Fabr{\'\i}cio~Gomes de et al. Otimiza{\c{c}}{\~a}o em teste de
  software com aplica{\c{c}}{\~a}o de metaheur{\'\i}sticas.
\emph{Revista de Sistemas de Informa{\c{c}}{\~a}o da FSMA}, n.~5, p. 3--13,
  2010.}

\bibitem[Isern, S{\'a}nchez e Moreno 2010]{isern2010agents}
\abntrefinfo{Isern, S{\'a}nchez e Moreno}{ISERN; S{\'A}NCHEZ; MORENO}{2010}
{ISERN, David; S{\'A}NCHEZ, David; MORENO, Antonio. Agents applied in health
  care: A review.
\emph{International Journal of Medical Informatics}, Elsevier, v.~79, n.~3, p.
  145--166, 2010.}

\bibitem[Jennings, Jennings e Wooldridge 1998]{jennings1998agent}
\abntrefinfo{Jennings, Jennings e Wooldridge}{JENNINGS; JENNINGS;
  WOOLDRIDGE}{1998}
{JENNINGS, Nick; JENNINGS, Nicholas~R; WOOLDRIDGE, Michael~J. \emph{Agent
  technology: foundations, applications, and markets}. [S.l.]: Springer, 1998.}

\bibitem[Ladeira 2011]{ladeira2011}
\abntrefinfo{Ladeira}{LADEIRA}{2011}
{LADEIRA, F. \emph{Acesso e Qualidade! Atenção básica ajusta foco em sua
  missão}. [S.l.]: Revista Brasileira de Saúde da Família, Brasília, n.29,
  p. 31-37, 2011.}

\bibitem[Oliveira et al. 2012]{oliveira}
\abntrefinfo{Oliveira et al.}{OLIVEIRA et al.}{2012}
{OLIVEIRA, {\'E}dson das Neves et al. Aplicativo auxiliando a melhoria da
  aten{\c{c}}{\~a}o b{\'a}sica no brasil application to assist in improving of
  health in brazil’s primary care.
2012.}

\bibitem[Russell 2003]{russell2003artificial}
\abntrefinfo{Russell}{RUSSELL}{2003}
{RUSSELL, Stuart. \emph{Artificial intelligence: A modern approach, 2/E}.
  [S.l.]: Pearson Education India, 2003.}

\bibitem[Russell e Norvig 2003]{russell2003inteligencia}
\abntrefinfo{Russell e Norvig}{RUSSELL; NORVIG}{2003}
{RUSSELL, Stuart; NORVIG, Peter. Intelig{\^e}ncia artificial: uma abordagem
  moderna. ed.
\emph{Campus, 2{\textordfeminine} Edi{\c{c}}{\~a}o. S{\~a}o Paulo, Brazil},
  2003.}

\bibitem[Sichman 2003]{sichman2003raciocinio}
\abntrefinfo{Sichman}{SICHMAN}{2003}
{SICHMAN, JS. Racioc{\'\i}nio social e organizacional em sistemas multiagentes:
  avan{\c{c}}os e perspectivas.
\emph{Universidade de S{\~a}o Paulo, S{\~a}o Paulo}, 2003.}

\bibitem[Silva et al. 2012]{silvaferramenta}
\abntrefinfo{Silva et al.}{SILVA et al.}{2012}
{SILVA, R{\^o}mulo~C et al. Ferramenta computacional para programa de melhoria
  da aten{\c{c}}{\~a}o b{\'a}sica (pmaq-ab).
2012.}

\bibitem[SILVEIRA e MOURA 2003]{silveira2003aplicaccao}
\abntrefinfo{SILVEIRA e MOURA}{SILVEIRA; MOURA}{2003}
{SILVEIRA, Rafael~L; MOURA, Arnaldo~V. Aplica{\c{c}}{\~a}o de heur{\'\i}sticas
  de busca local ao problema de agendamento escolar.
\emph{Relat{\'o}rio T{\'e}cnico IC-03-28, Instituto de
  Computa{\c{c}}{\~a}o--Unicamp}, 2003.}

\bibitem[Vermeulen et al. 2009]{vermeulen2009adaptive}
\abntrefinfo{Vermeulen et al.}{VERMEULEN et al.}{2009}
{VERMEULEN, Ivan~B et al. Adaptive resource allocation for efficient patient
  scheduling.
\emph{Artificial intelligence in medicine}, Elsevier, v.~46, n.~1, p. 67--80,
  2009.}

\bibitem[Wooldridge 2008]{wooldridge2008introduction}
\abntrefinfo{Wooldridge}{WOOLDRIDGE}{2008}
{WOOLDRIDGE, Michael. \emph{An introduction to multiagent systems}. [S.l.]:
  Wiley. com, 2008.}

\end{thebibliography}
