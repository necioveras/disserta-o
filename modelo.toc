\select@language {portuges}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{12}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{12}
\contentsline {section}{\numberline {1.2}Relev\IeC {\^a}ncia}{14}
\contentsline {section}{\numberline {1.3}Objetivos}{15}
\contentsline {section}{\numberline {1.4}Organiza\IeC {\c c}\IeC {\~a}o do trabalho}{16}
\contentsline {chapter}{\numberline {2}Referencial Te\IeC {\'o}rico}{17}
\contentsline {section}{\numberline {2.1}O Sistema \IeC {\'U}nico de Sa\IeC {\'u}de, Aten\IeC {\c c}\IeC {\~a}o B\IeC {\'a}sica e o PMAQ}{17}
\contentsline {section}{\numberline {2.2}Conceitos e fundamentos dos agentes inteligentes}{19}
\contentsline {subsection}{\numberline {2.2.1}Arquiteruas de agentes}{21}
\contentsline {subsection}{\numberline {2.2.2}Ambientes de agentes}{21}
\contentsline {subsection}{\numberline {2.2.3}Sistemas multiagentes}{21}
\contentsline {section}{\numberline {2.3}Otimiza\IeC {\c c}\IeC {\~a}o Combinat\IeC {\'o}ria}{22}
\contentsline {subsection}{\numberline {2.3.1}Metaheur\IeC {\'\i }sticas de busca local}{23}
\contentsline {section}{\numberline {2.4}Simula\IeC {\c c}\IeC {\~a}o baseada em agentes}{23}
\contentsline {chapter}{\numberline {3}Trabalhos Relacionados}{24}
\contentsline {section}{\numberline {3.1}Crit\IeC {\'e}rios de compara\IeC {\c c}\IeC {\~a}o entre a proposta e os trabalhos relacionados}{27}
\contentsline {chapter}{\numberline {4}O problema do planejamento de atendimentos na aten\IeC {\c c}\IeC {\~a}o prim\IeC {\'a}ria}{29}
\contentsline {chapter}{\numberline {5}Abordagem proposta}{30}
\contentsline {chapter}{\numberline {6}Simula\IeC {\c c}\IeC {\~o}es}{31}
\contentsline {chapter}{\numberline {7}Considera\IeC {\c c}\IeC {\~o}es finais}{32}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Refer\IeC {\^e}ncias}{33}
